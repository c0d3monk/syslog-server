# syslog-server

A syslog server utility tool written in Perl

It handles both UDP and TCP protocols.

## Install

* Download: [click here](https://bintray.com/akr-optimus/deb/download_file?file_path=pool%2Fs%2Fsyslog-server%2Fsyslog-server.deb)
or `curl -O "https://dl.bintray.com/akr-optimus/deb/pool/s/syslog-server/syslog-server.deb"`
* `dpkg -i syslog-server.deb`

## Execute:

* `sudo perl /usr/bin/syslog_server.pl`