#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

use IO::File;
use IO::Select;
use Getopt::Long;
use IO::Socket::INET6;

my $syslog_server;
my $now = gmtime;

my %options;

GetOptions(\%options, "proto=s", "port|p=i", "outputdir=s");

# Set the defaults

my $hostname = `hostname -f`;
my $port = $options{'port'} || 514;
my $proto = $options{'proto'} || 'udp';
my $output_dir = $options{'outputdir'} || "/tmp/syslog_dir";

my $log = setlog();

$SIG{TERM} = sub {
    print $log "Killed by SIGTERM";
    close $log;
    $syslog_server->close() if (defined $syslog_server);
    exit 255;
};

print $log "Starting the syslog server on $hostname:$port at $now (PID: $$)\n";

my @params = ('LocalPort', $port, 'LocalAddr', 'localhost');

($proto eq 'udp') ? push @params, ("Proto", 'udp', 'Type', SOCK_DGRAM) : push @params, ('Proto', 'tcp', 'Reuse', 1, 'Listen', 10, 'Type', SOCK_STREAM);

$syslog_server = IO::Socket::INET6->new(@params);

unless (defined $syslog_server) {
    print $log "Couldn't open $proto socket on port $port [$!]\n";
    print $log `netstat -nlp`;
    die;
}

my $counter = 1;
if ($proto eq 'udp') {
    handle_udp_connections();
} else {
    handle_tcp_connections();
}

# Handle UDP Connections
sub handle_udp_connections {
    my $event = undef;
    print $log "Waiting for client to connect using UDP\n";
    while (1) {
        $syslog_server->recv($event, 1024);
        record_event($event);
        $event = undef;
    }
}

# Handle TCP Connections

sub handle_tcp_connections {
    my $sel = IO::Select->new();
    $sel->add($syslog_server);
    print $log "Listener fd is " . fileno($syslog_server) . "\n";
    my @buf;
    while(1) {
        my @active_fds = $sel->can_read(10);
        print $log "Notified about " . scalar(@active_fds) ." fds at" . scalar(localtime) . "\n";
        foreach my $fd (@active_fds) {
            print $log "Handling fd " . fileno($fd) . "\n";
            my $fn = fileno($fd);
            if ($fn == fileno($syslog_server)) {
                my $client = $syslog_server->accept();
                $fn  = fileno($client);
                print $log "TCP connection from " . $client->peerhost() . ":" . $client->peerport(). " accepted as fd\n";
                $sel->add($client);
                $buf[$fn] = '';
            } else {
                print $log "Notified about an existing connection on $fn\n";
                my $ret = sysread($fd, $buf[$fn], 4096, length($buf[$fn]));
                print $log "read returned $ret, we have ".length($buf[$fn])." of bytes read from $fn\n";
                while($buf[$fn] =~ s/(.*?\n)//) {
                    record_event($1);
                }
                if (!$ret) {
                    print $log "Finished reading from client - closing connection\n";
                    my $len = length($buf[$fn]);
                    print $log "Discarding $len bytes of data" if ($len);
                    print $log "\n";
                    $buf[$fn] = '';
                    $sel->remove($fd);
                    close $fd;
                }
            }
        }
    }
}

# Record events
sub record_event {
    my $event = shift;
    print $log "Received event from client: $event\n";
    open(my $ev_fh, ">$output_dir/syslog_event.$counter") or die $!;
    print $ev_fh $event;
    close $ev_fh;
    $counter++;
}

# Set the logging configuration
sub setlog {
    system("mkdir -p $output_dir");
    my $logger = IO::File->new($output_dir."/syslog_server.log", ">");
    open STDOUT, ">&=", $logger;
    STDOUT->autoflush(1);
    open STDERR, ">&=", $logger;
    STDERR->autoflush(1);
    $logger->autoflush(1);
    return $logger;
}